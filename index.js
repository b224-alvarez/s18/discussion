// console.log("Hello Batch 224!");

// FUNCTIONS

// Parameters and Arguments

// Function Declaration without the use of parameters and arguments
/* function printInput() {
  let nickName = prompt("Enter your nickname: ");
  console.log("Hi, " + nickName);
}
printInput();
 */

// Function Declaration using arguments and params

function printName(firstName, lastName) {
  console.log("My name is " + firstName + " " + lastName);
}

printName("Levin", "Alvarez");

// You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

// "name" in our example is called a parameter.
// A "parameter" acts as a named variable/container that exists only inside a function.
// It is used to store information that is provided to a function when it is called or invoked.

// When the "printName()" function is first called, it stores the value of "Levin" in the parameter "name" then uses it to print a message.
// When the "printName()" function is called again, it then stores the value of "Jack" in the parameter "name" then uses it to print a message.
// Same goes with the third invocation of the "printName()" function.
printName("Jack");
printName("Rose");

// Variables can also be passed as an argument

let sampleVar = "Mark";

printName(sampleVar);

printName(sampleVar, "Justin"); //Justin will be ignored by the function "printName".
// Function arguments cannot be used by a function if there are no parameters provided within the function.

// Now, you can create a function which can be re-used to check for a number's divisibility instead of having to manually do it everytime like in our previous activity.
function checkDivBy8(num) {
  let remainder = num % 8;
  console.log("The remainder of " + num + " divided by 8 is: " + remainder);
  let isDivisibleBy8 = remainder === 0;
  console.log("Is " + num + " divisible by 8?");
  console.log(isDivisibleBy8);
}

checkDivBy8(64);
checkDivBy8(28);
checkDivBy8(37);

// Functions as Arguments

// Function parameters can also accpet other functions as arguments
// Some complex functions use other functions as arguments to perform more complicated results.

function argumentFunction() {
  console.log(
    "This function was pased as an argument before the message was printed"
  );
}

function invokeFunction(argumentFunction) {
  argumentFunction();
}

// Adding and removing the parenthesis "()" impacts the output of JavaScript.
// When a function is used with a parenthesis, it denotes invoking/calling a function.
// A function used without a parenthesis is normally associated with using a funciton as an argument to another function.
invokeFunction(argumentFunction);

argumentFunction();

// usee the console to find more information about the function.
console.log(argumentFunction);

// Using Multiple Parameters

// Multiple "arguments" that will correspond to the number of "parameters" declared in a function in a succeeding order.

function createFullName(firstName, middleName, lastName) {
  console.log(
    "My Full name is: " + firstName + " " + middleName + " " + lastName + "."
  );
}

createFullName("Levin John", "Bendijo", "Alvarez");

// "Levin John" will be stored in the parameter "firstName"
// "Bendijo" will be stored in the parameter "middleName"
// "Alvarez" will be stored in the parameter "lastName"

// In JS, providing more/less arguments than the expected parameters will not return an error.
// Providing less arguments than the expected parameters will automatically assign an "undefined" value to the parameter.
// In other programming languages, this will return an error stating that "The expected number of arguments do not match the number of parameters."
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Hello");

// Using Multiple Variables as Arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

// The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, the second argument will be stored in the second parameter and so on.

// The Return Statement

// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked or called the function.

function returnFullName(firstName, middleName, lastName) {
  return firstName + " " + middleName + " " + lastName + ".";
  console.log("This message will not be printed");
}

// returnFullName("Jeffrey", "Smith", "Bezos"); // simply invoking the "returnFullName" function will not print the message in the console.

// In our example, the "returnFullName" function was invoked/called in  the same line as declaring a variable.
// Whatever value is returned from the "returnedFullName" function is stored in the "completeName"
let completeName = returnFullName("Jeffrey", "Smith", "Bezoz");

console.log(completeName);
// This was, a function that was able to return a value that we can further use/manipulate in our programs instead of simply printing / displaying it in the console.

// In this example, console.log() will print the returned value of the returnFullName function.
console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain the result and return the value of the variable instead.
function returnAddress(city, country) {
  let fullAddress = city + ", " + country;
  return fullAddress;
}

let myAddress = returnAddress("Zamboanga", "Philippines");
console.log(myAddress);
console.log(returnAddress("Zamboanga", "Philippines"));

// On the other hand, when a function only has console.log() to display its results/value it will return undefined instead.
function printPlayerInfo(username, level, job) {
  console.log("Username: " + username);
  console.log("Level: " + level);
  console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", "80", "Paladin");
console.log(user1);
